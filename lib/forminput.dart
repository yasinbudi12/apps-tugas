import 'package:flutter/material.dart';

class Formini extends StatefulWidget {
  @override
  _ForminiState createState() => _ForminiState();
}

class _ForminiState extends State<Formini> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form input"),
      ),
      body: ListView(
        children: [
          Text(
            "Name",
            style: TextStyle(fontSize: 20),
          ),
          TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30))),
          ),
          Text(
            "email",
            style: TextStyle(fontSize: 20),
          ),
          TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30))),
          ),
          Text(
            "Distribution Filer",
            style: TextStyle(fontSize: 20),
          ),
          TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30))),
          ),
          RaisedButton(
            onPressed: () {},
            child: Text("Save From"),
          )
        ],
      ),
    );
  }
}
