import 'package:flutter/material.dart';

class ListFile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List File"),
      ),
      body: ListView.builder(
          itemCount: 30,
          itemBuilder: (context, index) {
            return ListTile(
              trailing: Icon(Icons.arrow_right),
              leading: Icon(Icons.brightness_1_rounded),
              title: Text("File $index"),
            );
          }),
    );
  }
}

class ListCompany extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List Company"),
      ),
      body: ListView.builder(
          itemCount: 30,
          itemBuilder: (context, index) {
            return ListTile(
              trailing: Icon(Icons.arrow_right),
              leading: Icon(Icons.brightness_1_rounded),
              title: Text("Company $index"),
            );
          }),
    );
  }
}
