import 'package:flutter/material.dart';
import 'package:tugas01/forminput.dart';
import 'package:tugas01/listfile.dart';
import 'package:tugas01/main.dart';

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.25),
            child: Center(
              child: Column(
                children: [
                  Image(
                    image: AssetImage("images/foto.png"),
                  ),
                  Text(
                    "Yasin budi Prasetya",
                    style: TextStyle(fontSize: 28),
                  ),
                  Text(
                    "20180801167",
                    style: TextStyle(fontSize: 20),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Formini()));
                            },
                            child: Container(
                              margin: EdgeInsets.all(10),
                              height: 100,
                              width: 100,
                              // color: Colors.amber,
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.add_circle,
                                  size: 50,
                                ),
                              ),
                            ),
                          ),
                          Text("Form Input"),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ListFile()));
                            },
                            child: Container(
                              margin: EdgeInsets.all(10),
                              height: 100,
                              width: 100,
                              // color: Colors.amber,
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.add_circle,
                                  size: 50,
                                ),
                              ),
                            ),
                          ),
                          Text("History Send"),
                        ],
                      ),
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ListFile()));
                            },
                            child: Container(
                              margin: EdgeInsets.all(10),
                              height: 100,
                              width: 100,
// color: Colors.amber,
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.add_circle,
                                  size: 50,
                                ),
                              ),
                            ),
                          ),
                          Text("list File"),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ListCompany()));
                            },
                            child: Container(
                              margin: EdgeInsets.all(10),
                              height: 100,
                              width: 100,
// color: Colors.amber,
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.add_circle,
                                  size: 50,
                                ),
                              ),
                            ),
                          ),
                          Text("list Company"),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          // Text("menu awalan")
        ],
      ),
    );
  }
}
